import java.util.Scanner;

public class GameXO {

	static Scanner kb = new Scanner(System.in);
	
	static char[][]board = {
							{' ','1','2','3'},
							{'1','-','-','-'},
							{'2','-','-','-'},
							{'3','-','-','-'}
							};
	 
	static char player = 'X';
	static int results = 0;
	static int turn = 1;
	static int checkwin = 0;
	
	public static void printwelcome() {
		System.out.println("Welcome to OX Game");
	}
	
	public static void switchplayer() {
		if(player == 'X') {
			player = 'O';
		}else {
			player = 'X';
		}
		turn++;
		
	}
	
	public static void printboard() {
		for(int row = 0;row<board.length;row++) {
			for(int col = 0;col<board[row].length;col++) {
				System.out.print(board[row][col]+" ");
			}System.out.println();
		}
	}
	
	public static void input() {
		System.out.println(player + "(R,C):");
		try {
		
		while (true) {		
			int r = kb.nextInt();
			int c = kb.nextInt();
			
			if(board[r][c]=='-') {
				board[r][c]=player;
				break;
			}
		}
		}catch(Exception e) {
			System.out.println("eror");
		}
		
		
	}
	
	public static void checkwinner() {
		Iwin();
		_win();
		Xwin();
		checkdraw();
	}
	
	public static void checkdraw() {
		if(turn==9)
			checkwin=1;
	}
	
	public static void Iwin() {
		if((board[1][1]==board[2][1]&&board[1][1]==board[3][1]&&board[1][1]!='-')||  
				(board[1][2]==board[2][2]&&board[1][2]==board[3][2]&&board[1][2]!='-')||
				(board[1][3]==board[2][3]&&board[1][3]==board[3][3]&&board[1][3]!='-')){
				if(player=='X') {
					results=1;
				}else {
					results=2;
				}
				checkwin = 1;;
			}
	}
	
	public static void _win() {
		if((board[1][1]==board[1][2]&&board[1][1]==board[1][3]&&board[1][1]!='-')||
				(board[2][1]==board[2][2]&&board[2][1]==board[2][3]&&board[2][1]!='-')||
				(board[3][1]==board[3][2]&&board[1][1]==board[3][3]&&board[3][1]!='-')){
				if(player=='X') {
					results=1;
				}else {
					results=2;
				}
				
				checkwin = 1;
			}
	}
	
	public static void Xwin() {
		if((board[1][1]==board[2][2]&&board[1][1]==board[3][3]&&board[1][1]!='-')||
				(board[3][1]==board[2][2]&&board[1][3]==board[3][1]&&board[3][1]!='-')){
					if(player=='X') {
						results=1;
					}else {
						results=2;
					}
					checkwin = 1;
			}
	}
	
	public static void printwin() {
		if(results == 1) {
			System.out.println("X win");
		}else if(results ==2) {
			System.out.println("O Win");
		}else { 
			System.out.println("Draw");
		}
	}
	
	public static void main(String[] args) {
		 printwelcome();
		 while(true) {
			 printboard();
			 input();
			 checkwinner();
			 if(checkwin == 1) {
				 break;
			 }
			 switchplayer();
		 }
		 
		 printboard();
		 printwin();
	}
}
